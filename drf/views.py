import status as status
from django.contrib.auth.models import User
from rest_framework import viewsets, status
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ParseError
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from BlogApp.models import Category, Post, Comment
from drf.serializers import CategorySerializer, UserSerializer, PostSerializer, CommentSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def create(self, request, *args, **kwargs):

        if request.user.is_superuser:
            serializer = CategorySerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"detail": "NOT FOUND..."}, status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        if request.user.is_superuser:
            try:
                cat_other = Category.objects.get(title='Others')
            except Category.DoesNotExist:
                cat_other = Category.objects.create(title='Others')

            post = Post.objects.filter(post_of_category=self.kwargs.get('pk'))
            for pst in post:
                pst.post_of_category = cat_other
                pst.save()

            instance = self.get_object()
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response({"detail": "NOT FOUND..."}, status=status.HTTP_404_NOT_FOUND)


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def create(self, request, *args, **kwargs):

        if request.user.is_authenticated():
            serializer = PostSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"detail": "NOT FOUND..."}, status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        post = Post.objects.get(id=kwargs.get('pk'))
        if request.user.is_authenticated() and request.user.id == post.author_id or request.user.is_superuser:
            instance = self.get_object()
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response({"detail": "NOT FOUND..."}, status=status.HTTP_404_NOT_FOUND)

    def update(self, request, *args, **kwargs):

        post = Post.objects.get(id=kwargs.get('pk'))
        if request.user.is_authenticated() and request.user.id == post.author_id or request.user.is_superuser:
            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            serializer = self.get_serializer(instance, data=request.data, partial=partial)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response({"detail": "NOT FOUND..."}, status=status.HTTP_404_NOT_FOUND)


class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def create(self, request, *args, **kwargs):

        if request.user.is_authenticated():
            serializer = CommentSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"detail": "NOT FOUND..."}, status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        comment = Comment.objects.get(id=kwargs.get('pk'))
        if request.user.is_authenticated() and request.user.id == comment.author_id or request.user.is_superuser:
            instance = self.get_object()
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response({"detail": "NOT FOUND..."}, status=status.HTTP_404_NOT_FOUND)

    def update(self, request, *args, **kwargs):

        comment = Comment.objects.get(id=kwargs.get('pk'))
        if request.user.is_authenticated() and request.user.id == comment.author_id or request.user.is_superuser:
            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            serializer = self.get_serializer(instance, data=request.data, partial=partial)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response({"detail": "NOT FOUND..."}, status=status.HTTP_404_NOT_FOUND)


class PostLikeViewSet(viewsets.ViewSet):
    def get(self, request, id=None):

        if id:
            post = Post.objects.get(id=id)
            post.num_of_like += 1
            post.save()
            data = PostSerializer(Post.objects.get(id=id)).data
            # content = {'post': data}
            return Response(data, status=status.HTTP_200_OK)
        else:
            return Response({"detail": "NOT FOUND..."}, status=status.HTTP_404_NOT_FOUND)


class PostDisLikeViewSet(viewsets.ViewSet):
    def get(self, request, id=None):

        if id:
            post = Post.objects.get(id=id)
            post.num_of_dislike += 1
            post.save()
            data = PostSerializer(Post.objects.get(id=id)).data
            return Response(data, status=status.HTTP_200_OK)
        else:
            return Response({"detail": "NOT FOUND..."}, status=status.HTTP_404_NOT_FOUND)


class CommentLikeViewSet(viewsets.ViewSet):
    def get(self, request, id=None):

        if id:
            comment = Comment.objects.get(id=id)
            comment.number_of_like += 1
            comment.save()
            data = CommentSerializer(Comment.objects.get(id=id)).data
            return Response(data, status=status.HTTP_200_OK)
        else:
            return Response({"detail": "NOT FOUND..."}, status=status.HTTP_404_NOT_FOUND)


class CommentDisLikeViewSet(viewsets.ViewSet):
    def get(self, request, id=None):

        if id:
            comment = Comment.objects.get(id=id)
            comment.number_of_dislike += 1
            comment.save()
            data = CommentSerializer(Comment.objects.get(id=id)).data
            return Response(data, status=status.HTTP_200_OK)
        else:
            return Response({"detail": "NOT FOUND..."}, status=status.HTTP_404_NOT_FOUND)


class TestView(viewsets.ViewSet):
    def get(self,request, format = None):
        return Response({'detail': "GET RESPONSE"})



class AuthView(viewsets.ViewSet):

    def get(self,request,format=None):
        return Response({'detail':"Authenticated"})

    def post(self, request, format=None):
        try:
            data = request.data
        except ParseError as error:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        if not data.get('username') or not data.get('password'):
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        user = User.objects.get(username=data.get('username'))
        if not user:
            return Response(status=status.HTTP_404_NOT_FOUND)

        token = Token.objects.get_or_create(user=user)
        return Response({'detail': 'POST answer', 'token': token[0].key})

    def destroy(self, request):
        try:
            data = request.data
        except ParseError as error:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        if User.objects.get(username=data.get('username')):
            token = Token.objects.get(user=User.objects.get(username=data.get('username')))
            token.delete()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)






