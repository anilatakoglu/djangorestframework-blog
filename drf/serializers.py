from django.contrib.auth.models import User
from rest_framework import serializers

from BlogApp.models import Category, Comment, Post


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        #fields = ('id', 'post_title', 'post_text', 'num_of_like', 'num_of_dislike')
        fields = '__all__'

class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        #fields = ('id', 'comment_text','number_of_like','number_of_dislike')
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')
