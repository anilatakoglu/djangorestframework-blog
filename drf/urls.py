from django.conf.urls import url, include
from rest_framework import routers

from drf import views

router = routers.DefaultRouter()
router.register(r'user', views.UserViewSet, base_name="user")
router.register(r'category', views.CategoryViewSet, base_name="category")
router.register(r'post', views.PostViewSet, base_name="post")
router.register(r'comment', views.CommentViewSet, base_name="comment")


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^post/(?P<id>[0-9]+)/like/$', views.PostLikeViewSet.as_view({'get': 'get'}), name='post_like'),
    url(r'^post/(?P<id>[0-9]+)/dislike/$', views.PostDisLikeViewSet.as_view({'get': 'get'}), name='post_dislike'),
    url(r'^comment/(?P<id>[0-9]+)/like/$', views.CommentLikeViewSet.as_view({'get': 'get'}), name='comment_like'),
    url(r'^comment/(?P<id>[0-9]+)/dislike/$', views.CommentDisLikeViewSet.as_view({'get': 'get'}),
        name='comment_dislike'),
    url(r'^auth/$', views.AuthView.as_view({'post': 'post', 'delete': 'destroy'}), name='auth_view'),

]
