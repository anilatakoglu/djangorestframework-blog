"""Blog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from BlogApp import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^admin/', admin.site.urls),
    url(r'^all_posts/$', views.PostView.as_view(), name='all_posts'),
    url(r'^category/(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^category/(?P<id>[0-9]+)/post/(?P<pk>[0-9]+)/$', views.ReadMore.as_view(), name='read_more'),
    url(r'^register/$', views.UserRegister.as_view(), name='register'),
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
    url(r'^add_category/$', views.AddCategory.as_view(), name='add_category'),
    url(r'^category/(?P<pk>[0-9]+)/add_post/$', views.AddPost.as_view(), name='add_post'),
    url(r'^category/(?P<id>[0-9]+)/post/(?P<pk>[0-9]+)/post_like/$', views.PostLike.as_view(), name='post_like'),
    url(r'^category/(?P<id>[0-9]+)/post/(?P<pk>[0-9]+)/post_dislike/$', views.PostDisLike.as_view(),
        name='post_dislike'),
    url(r'^category/(?P<id>[0-9]+)/post/(?P<pk>[0-9]+)/comment_like/(?P<comment_id>[0-9]+)/$', views.CommentLike.as_view(),
        name='comment_like'),
    url(r'^category/(?P<id>[0-9]+)/post/(?P<pk>[0-9]+)/comment_dislike/(?P<comment_id>[0-9]+)/$',
        views.CommentDisLike.as_view(),
        name='comment_dislike'),
    url(r'^category/(?P<id>[0-9]+)/post/(?P<pk>[0-9]+)/comment_delete/(?P<comment_id>[0-9]+)/$',
        views.DeleteComment.as_view(),
        name='delete_comment'),
    url(r'^category/(?P<id>[0-9]+)/post_delete/(?P<pk>[0-9]+)/$', views.DeletePost.as_view(), name='delete_post'),
    url(r'^category/(?P<id>[0-9]+)/update_post/(?P<pk>[0-9]+)/$', views.UpdatePost.as_view(), name='update_post'),
    url(r'^category/(?P<id>[0-9]+)/post/(?P<pk>[0-9]+)/update_comment/(?P<comment_id>[0-9]+)/$',
        views.UpdateComment.as_view(),
        name='update_comment'),
    url(r'^profile/(?P<id>[0-9]+)/$', views.ProfileView.as_view(), name='profile'),
    url(r'^delete_category/(?P<pk>[0-9]+)/$', views.DeleteCategory.as_view(), name='delete_category'),
    url(r'^api/', include('drf.urls', namespace='rest_framework'))
]
