from django import forms
from django.contrib.auth.models import User

from BlogApp.models import Category, Post, Comment


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'email', 'password')


class LoginForm(forms.Form):
    username = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput())


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['title']


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('post_title', 'post_text')


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['comment_text']