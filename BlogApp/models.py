from __future__ import unicode_literals


from django.contrib.auth.models import User
from django.db import models

# Create your models here.

from django.utils.six import python_2_unicode_compatible



@python_2_unicode_compatible
class Category(models.Model):
    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title


@python_2_unicode_compatible
class Post(models.Model):
    author = models.ForeignKey(User, default='')
    post_of_category = models.ForeignKey(Category)

    post_title = models.TextField(max_length=200)
    post_text = models.TextField(max_length=500)
    num_of_like = models.IntegerField(default=0)
    num_of_dislike = models.IntegerField(default=0)
    created_date = models.DateTimeField( auto_now_add=True)
    updated_date = models.DateTimeField( auto_now_add=True)

    def __str__(self):
        return self.post_text


@python_2_unicode_compatible
class Comment(models.Model):
    author = models.ForeignKey(User, default='')
    post_of_comment = models.ForeignKey(Post)

    comment_text = models.TextField(max_length=200)
    number_of_like = models.IntegerField(default=0)
    number_of_dislike = models.IntegerField(default=0)
    created_date = models.DateTimeField( auto_now_add=True)
    updated_date = models.DateTimeField( auto_now_add=True)

    def __str__(self):
        return self.comment_text
