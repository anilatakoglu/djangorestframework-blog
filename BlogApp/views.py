# Create your views here.
import user
from audioop import reverse

import datetime
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.views import generic

from BlogApp.forms import UserForm, LoginForm, CategoryForm, PostForm, CommentForm
from BlogApp.models import Category, Post, Comment


class IndexView(generic.DetailView):
    template_name = 'index.html'

    # context_object_name = 'categories'

    def get(self, request):
        categories = Category.objects.all()
        posts = Post.objects.order_by('-created_date')[:5]

        return render(request, 'index.html', {
            'categories': categories,
            'posts': posts
        })


class PostView(generic.DetailView):
    template_name = 'post.html'

    def get(self, request):
        # categories = Category.objects.all()
        posts = Post.objects.order_by('-created_date')

        return render(request, 'post.html', {
            'posts': posts
        })


class DetailView(generic.View):
    model = Category

    def get(self, request, pk):
        category = Category.objects.get(pk=pk)
        return render(request, 'detail.html', {
            'category': category})


class UserRegister(generic.View):
    def get(self, request):
        user_form = UserForm()
        return render(request, 'register.html', {'user_form': user_form})

    def post(self, request):
        user_form = UserForm(request.POST)
        if user_form.is_valid():
            user = User.objects.create(username=user_form.data.get('username'), email=user_form.data.get('email'))
            user.set_password(user_form.data.get('password'))
            user.save()

            return HttpResponseRedirect('/')

        else:

            return render(request, 'register.html', {
                'user_form': user_form,
                'errors': user_form.errors

            })

    template_name = "register.html"
    form_class = UserForm
    success_url = '/'


class LoginView(generic.View):
    def get(self, request):
        login_form = LoginForm()
        return render(request, 'login.html', {'login_form': login_form, 'errors': login_form.errors
                                              })

    def post(self, request):
        login_form = LoginForm(request.POST)

        if login_form.is_valid():
            user = authenticate(username=login_form.data.get('username'), password=login_form.data.get('password'))

            if user:
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                return render(request, 'login.html', {
                    'login_form': login_form,
                    'errors': login_form.errors

                })
        else:
            return render(request, 'login.html', {
                'user_form': login_form,
                'errors': login_form.errors

            })

    template_name = "login.html"
    form_class = LoginForm
    success_url = '/'


7


class LogoutView(generic.View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/')


class AddCategory(generic.View):
    def get(self, request):
        if request.user.is_authenticated() and request.user.is_superuser:
            category_form = CategoryForm()
            return render(request, 'add_category.html', {'category_form': category_form})
        else:
            return HttpResponseRedirect('/login/')

    def post(self, request):
        category_form = CategoryForm(request.POST)

        if category_form.is_valid():
            category_form.save()
            return HttpResponseRedirect('/')
        else:
            return render(request, 'add_category.html',
                          {'category_form': category_form, 'errors': category_form.errors})

    template_name = "add__category.html"
    form_class = CategoryForm
    success_url = '/'


class AddPost(generic.View):
    def get(self, request, pk):
        if request.user.is_authenticated():
            post_form = PostForm()
            return render(request, 'add_post.html', {'post_form': post_form})
        else:
            return HttpResponseRedirect('/login/')

    def post(self, request, pk):

        post_form = PostForm(request.POST)
        category = Category.objects.get(pk=pk)

        if post_form.is_valid():
            post_form.save(commit=False)
            post_form.instance.post_of_category = category
            post_form.instance.author = request.user
            post_form.save()

            return HttpResponseRedirect('/category/%s/' % self.kwargs.get('pk'))

        else:
            return render(request, 'add_post.html', {
                'post_form': post_form,
                'errors': post_form.errors

            })

    template_name = "add_post.html"
    form_class = PostForm
    success_url = '/'


class UpdatePost(generic.View):
    def get(self, request, id, pk):
        post = Post.objects.get(pk=self.kwargs.get('pk'))
        if request.user.is_authenticated() and request.user.id == post.author_id or request.user.is_superuser:
            post_form = PostForm()
            return render(request, 'update_post.html', {'post_form': post_form})
        else:
            return HttpResponseRedirect('/login/')

    def post(self, request, id, pk):
        post_form = PostForm(request.POST)
        if post_form.is_valid():

            post = Post.objects.get(pk=self.kwargs.get('pk'))
            post.post_title = post_form.instance.post_title
            post.post_text = post_form.instance.post_text
            post.updated_date = datetime.datetime.now().replace(microsecond=0)
            post.save()
            return HttpResponseRedirect('/category/%s/post/%s/' % (self.kwargs.get('id'), self.kwargs.get('pk')))

        else:
            return render(request, 'update_post.html', {
                'post_form': post_form,
                'errors': post_form.errors

            })

    template_name = "update_post.html"
    form_class = PostForm


class UpdateComment(generic.View):

    def get(self, request, id, pk, comment_id):
        comment = Comment.objects.get(id=self.kwargs.get('comment_id'))
        if request.user.is_authenticated() and request.user.id == comment.author_id or request.user.is_superuser:

            comment_form = CommentForm()
            return render(request, 'update_comment.html', {'comment_form': comment_form})
        else:
            return HttpResponseRedirect('/login/')

    def post(self, request, id, pk, comment_id):
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():

            comment = Comment.objects.get(id=self.kwargs.get('comment_id'))
            comment.comment_text = comment_form.instance.comment_text
            comment.updated_date = datetime.datetime.now().replace(microsecond=0)
            comment.save()

            return HttpResponseRedirect('/category/%s/post/%s/' % (self.kwargs.get('id'), self.kwargs.get('pk')))

        else:
            return render(request, 'update_comment.html', {
                'comment_form': comment_form,
                'errors': comment_form.errors

            })

    template_name = "update_comment.html"
    form_class = CommentForm


class ReadMore(generic.View):
    model = Post

    def get(self, request, pk, id):
        category = Category.objects.get(id=self.kwargs.get('id'))
        post = Post.objects.get(pk=self.kwargs.get('pk'))
        comment = Comment.objects.all()
        comment_form = CommentForm()
        return render(request, 'read_more.html',
                      {'comment_form': comment_form, 'post': post, 'category': category, 'comment': comment})

    def post(self, request, pk, id):

        comment_form = CommentForm(request.POST)
        category = Category.objects.get(id=id)
        post = Post.objects.get(pk=pk)
        comment = Comment.objects.all()

        if comment_form.is_valid():
            comment_form.save(commit=False)
            comment_form.instance.post_of_comment = post
            comment_form.instance.author = request.user
            comment_form.save()

            return HttpResponseRedirect('/category/%s/post/%s/' % (self.kwargs.get('id'), self.kwargs.get('pk')))

        else:
            return render(request, 'read_more.html', {
                'comment_form': comment_form,
                'post': post, 'category': category, 'comment': comment,
                'errors': comment_form.errors

            })

    template_name = "read_more.html"
    form_class = CommentForm
    success_url = '/'


class PostLike(generic.View):
    def get(self, request, id, pk):
        if request.user.is_authenticated():
            post = Post.objects.get(id=self.kwargs.get('pk'))
            post.num_of_like += 1
            post.save()
            return HttpResponseRedirect('/category/%s/post/%s/' % (self.kwargs.get('id'), self.kwargs.get('pk')))
        else:
            return HttpResponseRedirect('/login')


class PostDisLike(generic.View):
    def get(self, request, id, pk):
        if request.user.is_authenticated():
            post = Post.objects.get(id=self.kwargs.get('pk'))
            post.num_of_dislike += 1
            post.save()
            return HttpResponseRedirect('/category/%s/post/%s/' % (self.kwargs.get('id'), self.kwargs.get('pk')))
        else:
            return HttpResponseRedirect('/login')


class CommentLike(generic.View):
    def get(self, request, id, pk, comment_id):
        if request.user.is_authenticated():
            comment = Comment.objects.get(id=self.kwargs.get('comment_id'))
            comment.number_of_like += 1
            comment.save()
            return HttpResponseRedirect('/category/%s/post/%s/' % (self.kwargs.get('id'), self.kwargs.get('pk')))
        else:
            return HttpResponseRedirect('login')


class CommentDisLike(generic.View):
    def get(self, request, id, pk, comment_id):
        if request.user.is_authenticated():
            comment = Comment.objects.get(id=self.kwargs.get('comment_id'))
            comment.number_of_dislike += 1
            comment.save()
            return HttpResponseRedirect('/category/%s/post/%s/' % (self.kwargs.get('id'), self.kwargs.get('pk')))
        else:
            return HttpResponseRedirect('/login/')


class DeleteComment(generic.DeleteView):

    def get(self, request, *args, **kwargs):
        comment = Comment.objects.get(id=kwargs['comment_id'])
        if request.user.is_authenticated()and request.user.id == comment.author_id or request.user.is_superuser:

            comment.delete()
            return HttpResponseRedirect('/category/%s/post/%s/' % (self.kwargs.get('id'), self.kwargs.get('pk')))
        else:
            return HttpResponseRedirect('/login/')


class DeletePost(generic.DeleteView):
    def get(self, request, *args, **kwargs):
        post = Post.objects.get(pk=kwargs['pk'])
        if request.user.is_authenticated() and request.user.id == post.author_id or request.user.is_superuser:

            post.delete()
            return HttpResponseRedirect('/category/%s/' % self.kwargs.get('id'))
        else:
            return HttpResponseRedirect('/login/')


class DeleteCategory(generic.View):
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated() and request.user.is_superuser:
            category = Category.objects.get(id=kwargs['pk'])

            try:
                cat_other = Category.objects.get(title='Others')
            except Category.DoesNotExist:
                cat_other = Category.objects.create(title='Others')

            post = Post.objects.filter(post_of_category=self.kwargs.get('pk'))
            for pst in post:
                pst.post_of_category = cat_other
                pst.save()

            category.delete()
            return HttpResponseRedirect('/')

        else:
            return HttpResponseRedirect('/login/')


class ProfileView(generic.View):
    def get(self, request, id):
        member = User.objects.get(id=self.kwargs.get('id'))
        post = Post.objects.filter(author=self.kwargs.get('id'))
        comment = Comment.objects.filter(author=self.kwargs.get('id'))
        return render(request, 'profile.html', {
            'member': member, 'post': post, 'comment': comment})
