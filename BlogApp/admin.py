from django.contrib import admin

# Register your models here.
from BlogApp.models import Category, Post, Comment


class Category_Admin(admin.ModelAdmin):
    list_display = ('id', 'title')
    list_filter = ['title']


# class UserAdmin(admin.ModelAdmin):
#     list_display = ('id', 'username', 'password')
#     search_fields = ['username']



class PostAdmin(admin.ModelAdmin):
    list_display = ('id', 'post_of_category', 'post_title', 'post_text',
                    'num_of_like', 'num_of_dislike', 'created_date', 'updated_date')


class CommentAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'post_of_comment', 'comment_text', 'number_of_like', 'number_of_dislike', 'created_date',
        'updated_date')


admin.site.register(Category, Category_Admin)
# admin.site.register(Author)
admin.site.register(Post, PostAdmin)
admin.site.register(Comment, CommentAdmin)
